CREATE TABLE public.movie_objects
(
    id serial NOT NULL,
    name character varying(200) NOT NULL,
    contract_id integer,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.movie_objects
    OWNER to rvision;