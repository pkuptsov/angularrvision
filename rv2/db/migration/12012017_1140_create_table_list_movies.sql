CREATE TABLE public.movies_list
(
    id serial NOT NULL,
    video_id character varying(11) NOT NULL,
    video_name text,
    channel_name text,
    object_id integer,
    CONSTRAINT movies_list_id PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.movies_list
    OWNER to rvision;
COMMENT ON TABLE public.movies_list
    IS 'all unique movies';
    
    
CREATE UNIQUE INDEX movies_list_video_id
ON public.movies_list USING btree
(video_id COLLATE pg_catalog."default" varchar_ops DESC NULLS FIRST)
TABLESPACE pg_default;