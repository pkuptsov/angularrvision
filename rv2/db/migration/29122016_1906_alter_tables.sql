ALTER TABLE public.movies
    ADD COLUMN object_id integer;

ALTER TABLE public.movies
    ALTER COLUMN object_id TYPE integer ;
ALTER TABLE public.movies
    ALTER COLUMN object_id SET DEFAULT NULL;