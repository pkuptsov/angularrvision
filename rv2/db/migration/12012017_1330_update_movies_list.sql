UPDATE movies_list
SET video_name=subquery.vdname,
    channel_name=subquery.channelname
FROM (SELECT "Video_ID" AS vdid, "Video_Name" AS vdname, channelname
      FROM  movies) AS subquery
WHERE movies_list.video_id=subquery.vdid;


ALTER TABLE public.movies DROP COLUMN object_id;