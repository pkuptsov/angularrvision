(function() {
var app = angular.module('Rates', []);

// controllers
app.controller('RatesListController', ['$http', '$routeParams', function ($http ,  $routeParams){
  
  var ratesctrl = this;
  ratesctrl.list = [ ];
  ratesctrl.curPage = $routeParams.curPage;
  ratesctrl.pages = [ ];

  $http.get('/api/rates/' + ratesctrl.curPage + '.json').success(function(data) {
      ratesctrl.list  = data.list;
      ratesctrl.pages = data.pages;
  });

}]);

})();