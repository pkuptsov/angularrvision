(function() {
var app = angular.module('rvApp', [
    'ngRoute',
    'Users',
    'Companies',
    'Contracts',
    'Logs',
    'deleteFromList',
    'Rates',
    'Import',
    'Reports',
    'Objects',
    ]);
// config
  app.config(function($routeProvider) {
    $routeProvider.
      when('/users', {
        templateUrl: '/js/angular/views/user-list.view.html',
        controller: 'UserListController'
      }).
      when('/companies', {
        templateUrl: '/js/angular/views/company-list.view.html',
        controller: 'CompanyListController'
      }).
      when('/companies/:companyId', {
        templateUrl: '/js/angular/views/company-details.view.html',
        controller: 'CompanyDetailController',
      }).
      when('/contracts', {
        templateUrl: '/js/angular/views/contract-list.view.html',
        controller: 'ContractListController'
      }).
      when('/contracts/:companyId/:contractId', {
        templateUrl: '/js/angular/views/contract-details.view.html',
        controller: 'ContractDetailController',
      }).
        when('/logs/:curPage', {
        templateUrl: '/js/angular/views/logs-list.view.html',
        controller: 'LogsListController',
      }).
        when('/mel/:file_id', {
        templateUrl: '/js/angular/views/mel-list.view.html',
        controller: 'MelController',
      }).
        when('/rates/:curPage', {
        templateUrl: '/js/angular/views/rates-list.view.html',
        controller: 'RatesListController',
      }).
        when('/import/:random', {
        templateUrl: '/js/angular/views/import-list.view.html',
        controller: 'ImportListController',
      }).
        when('/movies/:curPage', {
        templateUrl: '/js/angular/views/movies-list.view.html',
        controller: 'MoviesListController',
      }).
        when('/reports', {
        templateUrl: '/js/angular/views/reports-filter.view.html',
        controller: 'ReportsController',
      }).
        when('/objects/add_new/:obj_id', {
        templateUrl: '/js/angular/views/objects-add_new.view.html',
        controller: 'ObjectsAddNewController',
      }).        
        when('/objects/:curPage', {
        templateUrl: '/js/angular/views/objects.view.html',
        controller: 'ObjectsController',
      }).
      //when('/objects/edit/:id', {
      //  templateUrl: '/js/angular/views/objects-edit.view.html',
      //  controller: 'ObjectsEditController',
      //}). 
      otherwise({
        redirectTo: '/movies/1'
      });
      
  });
})();
