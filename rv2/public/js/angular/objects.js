(function() {
var app = angular.module('Objects', []);

// controllers
app.controller('ObjectsController', ['$http', '$routeParams','$location', function ( $http, $routeParams, $location ){
  var objctrl = this;
  objctrl.table = [];
  objctrl.curPage = $routeParams.curPage;
  objctrl.current = {};
  objctrl.loading = true;
  $http.get('/api/objects/list/' + objctrl.curPage + '.json').success(function(data) {
    objctrl.loading = false;
    objctrl.table  = data.list;
    objctrl.pages = data.pages;
    console.log(objctrl.pages);
  });
  
  this.addObject = function(current) {
    if (current) {
      objctrl.current = current;
      $location.path('/objects/add_new/' + current.id);
    } else {
      $location.path('/objects/add_new/0');
    }
  };
    
    this.objectDelete = function(obj_id) {
    this.obj_id = obj_id;
    
    $http.post('/api/objects/delete/' + obj_id + '.json').success(function(data) {
       objctrl.status = true;
       objctrl.error = false;
       objctrl.table = data.list;
       objctrl.message = data.message;
       objctrl.pages = data.pages;
    }).error(function(data) {
       objctrl.status = true;
       objctrl.error = true;
       objctrl.message = data.message;
    });
    
  };
  
}]);

app.controller('ObjectsAddNewController', ['$http', '$routeParams', '$location', function ( $http, $routeParams, $location ){
  var objctrl = this;
  objctrl.current = {};
  objctrl.contracts = [];
  objctrl.table = [];
  objctrl.current.videos = [];
  objctrl.obj_id = $routeParams.obj_id;
  objctrl.loading = false;
  // если передан существующий объект - заполняем current
  if(objctrl.obj_id > 0) {
    objctrl.loading = true;
    $http.post('/api/objects/edit/' + objctrl.obj_id + '.json').success(function(data) {
    objctrl.loading = false;
    objctrl.current = data.table;
    objctrl.table = data.movies;
    // заполняем массив с видео id
    var arr = [];
    angular.forEach(objctrl.table, function(value, key) { 
      this.push(value.id);
    }, arr);
    // сохраняем в объекте
    objctrl.current.videos = arr;
  });
  }
  
  $http.get('/api/objects/contracts.json').success(function(data) {
      objctrl.contracts = data;
  });
  
  this.findMovies = function() {
    objctrl.loading = true;
    $http.post('/api/objects/filter?search='+ objctrl.current.filter).success(function(data) {
      objctrl.table  = data;
      objctrl.loading = false;
    });
  };
  
  this.objectAdd = function(current)
  {
    console.log(current.id);
    $http.post('/api/objects/add_new', current).success(function(data) {
       $location.path('/objects/1');
    });
  };
  
  this.objectCancel = function() {
    $location.path('/objects/1');
  };
  
  this.addToObject = function(id)
  {
    //console.log(objctrl.current.videos);
    var elem = objctrl.current.videos.indexOf(id);
    if(elem<0)
    {
      objctrl.current.videos.push(id);
    } else {
      objctrl.current.videos.splice(elem,1);
    }
  };
    
}]);

})();
