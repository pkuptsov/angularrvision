(function() {
var app = angular.module('Reports', []);

// controllers
app.controller('ReportsController', ['$http', '$location', function ( $http, $location ){
  var repctrl = this;
  repctrl.owners = [];
  repctrl.current = {};
  repctrl.table = [];
  repctrl.show = false; // показывать или нет элемент с таблицей
  repctrl.loading = false;
  $http.get('/api/objects/contracts.json').success(function(data) {
      repctrl.owners = data;
  });
  
  this.preformReport = function(current) {
    repctrl.show = true;
    repctrl.loading = true;
    repctrl.table = [];// сброс
   $http.post('/api/reports/report1.json', current).success(function(data) {
      repctrl.loading = false;
      repctrl.table = data;
    }).error(function(data) {
      repctrl.status = true;
      repctrl.error = true;
      repctrl.message = data.message;
    }); 
  };
  
}]);

})();
