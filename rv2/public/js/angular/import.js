(function() {
var app = angular.module('Import', []);

// controllers
app.controller('ImportListController', ['$http', '$location', '$interval', function ( $http, $location, $interval ){
  var impctrl = this;
  impctrl.filelist = [];
  impctrl.progressBar = {};
  impctrl.intervals = {};
    
  impctrl.startInterval = function(fileId){
      impctrl.intervals[fileId] = 
        $interval(function(){            
            $http.post('/api/progress/file/' + fileId).success(function(data){
                if (data.processed_count == data.total_count){
                    impctrl.stopInterval(fileID);
                } else {
                    impctrl.progressBar[fileId] = Math.round(100 *  data.processed_count / ( data.total_count || 1 ));
                }
            });
        }, 5000, 1440);
  }
  
  impctrl.stopInterval = function(fileId){
      
      if (angular.isDefined( impctrl.intervals[fileId] )) {
        $interval.cancel( impctrl.intervals[fileId] );
        impctrl.intervals[fileId] = undefined;
    }
  };
  
  $http.get('/api/import/file/list.json').success(function(data) {
      impctrl.filelist = data;
      for (i in impctrl.filelist){
                  
        if ( impctrl.filelist[i].status == 'api'){
            impctrl.startInterval(impctrl.filelist[i].id);
        }
        
      }
  });
  
  this.fileDelete = function(file_id) {
    this.file_id = file_id;
    
     $http.post('/api/delete/import/file/' + file_id).success(function(data) {
       $location.path('/import/' + Math.random().toString(10));
    }).error(function(data) {
      console.log(data);
    });
     
  }

  this.fileProcess = function(file_id) {
   this.file_id = file_id;

   $http.post('/api/process/file/' + file_id).success(function(data){
      impctrl.filelist = data;
        
   }).error(function(error) {
      console.log(error);
   });
  }

  this.showModal = function(file_id) {
    this.file_id = file_id;
    impctrl.loading = true;
    impctrl.statlist = {};
    $http.post('/api/statistic/file/' + file_id).success(function(data){
      impctrl.statlist = data;
      impctrl.loading = false;
   }).error(function(error) {
      console.log(error);
   });
     
  };
  
  this.reqApi = function(file_id) {
   this.file_id = file_id;

   $http.post('/api/process/req/' + file_id).success(function(data){
      impctrl.filelist = data;
      console.log('-- reqApi 2 --');
      impctrl.startInterval( file_id );
   }).error(function(error) {
      console.log(error);
   });
  }
  
}]);


app.controller('MoviesListController', ['$http', '$routeParams', function ( $http, $routeParams ){
  var movctrl = this;
  movctrl.movies = [];
  movctrl.curPage = $routeParams.curPage || 1;
  movctrl.pages = [];
  movctrl.showLoader = 1;
  movctrl.orderDirection = ($routeParams.order_direction == 'asc' ? 'desc' : 'asc');
  movctrl.order = $routeParams.order || 'Video_Name';
  movctrl.moviesOrderString = ($routeParams.order_direction == 'asc' ? '' : '-') + movctrl.order;
  
  $http.get('/api/movies/list/' + movctrl.curPage + '.json?order='+ $routeParams.order + '&order_direction=' + $routeParams.order_direction).success(function(data) {
      movctrl.movies = data.movies;
      movctrl.pages = data.pages;
      movctrl.total = data.total;
      movctrl.showLoader = 0;
  });
  
  this.searchBy = function(text){
    $http.get('/api/movies/list/' + movctrl.curPage + '.json?search='+text).success(function(data) {
      movctrl.movies  = data.movies;
      movctrl.pages = data.pages;
      movctrl.total = data.total;
    });
  }
  
}]);

})();
