(function() {
var app = angular.module('Logs', []);

app.controller('LogsListController',['$http', '$routeParams', function ( $http, $routeParams ){
    var logs = this;
    logs.list = [];
    logs.curPage = $routeParams.curPage || 1;
    logs.pages = [];

    $http.get('/api/logs/' + logs.curPage + '.json').success(function(data) {
        logs.list  = data.logs;
        logs.pages = data.pages; 
    });

    this.searchBy = function(text){
        $http.get('/api/logs/' + logs.curPage + '.json?search='+text).success(function(data) {
                  logs.list  = data.logs;
                  logs.pages = data.pages; 
              });
    }
  
}]);

app.controller('MelController',['$http', '$routeParams', function ( $http, $routeParams ){
    var logs = this;
    logs.list = [];
    logs.curPage = $routeParams.curPage || 1;
    logs.pages = [];

    $http.get('/api/mel/' + logs.curPage + '.json').success(function(data) {
        logs.list  = data.logs;
        logs.pages = data.pages; 
    });

    this.searchBy = function(text){
        $http.get('/api/mel/' + logs.curPage + '.json?search='+text).success(function(data) {
                  logs.list  = data.logs;
                  logs.pages = data.pages; 
              });
    }
  
}]);

})();
