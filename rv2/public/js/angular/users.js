(function() {
var app = angular.module('Users', []);

// controllers
app.controller('UserListController', ['$http', '$window', function ($http,$window){
  var userlist = this;
  
  userlist.users = [ ];
  userlist.current = {};
  userlist.status = false;
  userlist.companies = [ ];
  userlist.checkIP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
  userlist.error = false;
  
  $http.get('/api/companies.json').success(function(data) {
      userlist.companies = data;
  });
  

  $http.get('/api/users.json').success(function(data) {
    userlist.users = data;
  });
  
  
  this.userUpdate = function(current) {
    $http.post('/api/update/users.json', current).success(function(data) {
      userlist.status = true;
      userlist.error = false;
      userlist.message = data.message;
      userlist.current = {};
    }).error(function(data) {
      userlist.status = true;
      userlist.error = true;
      userlist.message = data.message;
    }); 
  };
  
  this.userCancel = function() {
    userlist.current = {};
  };
  
  this.userDelete = function(user_id) {
    this.user_id = user_id || 0;

    $http.post('/api/delete/users/' + user_id + '.json').success(function(data) {
       userlist.status = true;
       userlist.error = false;
       userlist.users = data.userlist;
       userlist.message = data.message;
    }).error(function(data) {
       userlist.status = true;
       userlist.error = true;
       userlist.message = data.message;
    });
    
  };
 
this.userAdd = function(newuser) {
  this.newuser = newuser;
     $http.post('/api/users.json', newuser).success(function(data) {
      userlist.status = true;
      userlist.error = false;
      userlist.message = data.message;
      newuser.id = data.id;
      userlist.users.push(newuser);
      userlist.newuser = {};
      
    }).error(function(data) {
      userlist.status = true;
      userlist.error = true;
      userlist.message = data.message;
    }); 
  };

  
  this.switchStatus = function() {
    userlist.status = false;
  };

}]);

})();