(function() {
var app = angular.module('Companies', []);

// controllers
app.controller('CompanyListController', ['$http', function ($http){
  var cmplist = this;
  
  cmplist.companies = [ ];
  cmplist.newcompany = {};
  cmplist.status = false;
  cmplist.error = false;
  cmplist.digitOnly =  /^\d+$/;
  
  $http.get('/api/companies.json').success(function(data) {
      cmplist.companies = data;
  });

  this.companyAdd = function(newcompany) {
  this.newcompany = newcompany;
     $http.post('/api/companies.json', newcompany).success(function(data) {
      cmplist.status = true;
      cmplist.error = false;
      cmplist.message = data.message;
      newcompany.id = data.id;
      cmplist.companies.push(newcompany);
      cmplist.newcompany = {};
      
    }).error(function(data) {
      cmplist.status = true;
      cmplist.error = true;
      cmplist.message = data.message;
    }); 
  };
  
  this.companyCancel = function() {
    cmplist.newcompany = {};
  };
  
  this.companyDelete = function(company_id) {
    this.company_id = company_id || 0;
    $http.post('/api/delete/companies/' + company_id + '.json').success(function(data) {
       cmplist.status = true;
       cmplist.error = false;
       cmplist.companies = data.companies;
       cmplist.message = data.message;
    }).error(function(data) {
      cmplist.status = true;
      cmplist.error = true;
      cmplist.message = data.message;
    });
    
  };

  this.switchStatus = function() {
    cmplist.status = false;
  }

}]);

app.controller('CompanyDetailController', ['$http', '$routeParams', '$location', function($http, $routeParams, $location) {
  var company = this;
  company.tab = 1;  
  company.current = {};
  company.status = false;
  company.banks = [];// list of accounts
  company.bank = {}; // edit account or new
  company.contracts = [];
  company.contract = {};
  company.error = false;
  company.digitOnly =  /^\d+$/;
  
  $http.get('/api/companies/' + $routeParams.companyId + '.json').success(function(data) {
       company.current = data;
   });
  
  this.selectTab = function(setTab) {
    company.tab = setTab;
  };
  
  this.isSelected = function(checkTab) {
    return company.tab === checkTab;
  };
  
  this.companyUpdate = function(current) {
  $http.post('/api/update/companies.json', current).success(function(data) {
      company.status = true;
      company.error = false;
      company.message = data.message;
    }).error(function(data) {
      company.status = true;
      company.error = true;
      company.message = data.message;
    });     
  };
  
  this.switchStatus = function() {
    company.status = false;
  };
  
  this.companyCancel = function() {
    $location.path('/companies');
  };
  
// ACCOUNTS 
  $http.get('/api/banks/' + $routeParams.companyId + '.json').success(function(data) {
      company.banks = data;
  });
  
// delete account
  this.accountDelete =  function(account_id) {
  
  this.account_id = account_id || 0;
  
  $http.post('/api/delete/banks/' + $routeParams.companyId + '/' + account_id + '.json').success(function(data) {
     company.status = true;
     company.error = false;
     company.banks = data.banks;
     company.message = data.message;
  }).error(function(data) {
    company.status = true;
    company.error = true;
    company.message = data.message;
  });
    
  };
  
  this.accountAdd = function(bank) {
    this.bank = bank;
    bank.company_id = $routeParams.companyId;
    $http.post('/api/banks.json', bank).success(function(data) {
      company.status = true;
      company.error = false;
      company.message = data.message;
      bank.id = data.id;
      company.banks.push(bank);
      company.bank = {};
      
    }).error(function(data) {
      company.status = true;
      company.error = true;
      company.message = data.message;
    }); 
  };
  
  this.accountEdit = function(bank) {
    this.bank = bank;
    $http.post('/api/update/banks.json', bank).success(function(data) {
      company.status = true;
      company.error = false;
      company.message = data.message;
      company.bank = {};
    }).error(function(data) {
      company.status = true;
      company.error = true;
      company.message = data.message;
    }); 
  };
  
  this.accountCancel = function() {
    company.bank = {};
  };
  
// CONTRACTS
  $http.get('/api/contracts/' + $routeParams.companyId + '.json').success(function(data) {
      company.contracts = data;
  });
  
  this.contractAdd = function(contract) {
    this.contract = contract;
    contract.company_id = $routeParams.companyId;
    $http.post('/api/contracts.json', contract).success(function(data) {
      company.status = true;
      company.error = false;
      company.message = data.message;
      contract.id = data.id;
      company.contracts.push(contract);
      company.contract = {};
    }).error(function(data) {
      company.status = true;
      company.error = true;
      company.message = data.message;
    }); 
  };
  
  
  this.contractDelete =  function(contract_id) {
  
      this.contract_id = contract_id || 0;
      
      $http.post('/api/delete/contracts/' + $routeParams.companyId + '/' + contract_id + '.json').success(function(data) {
         company.status = true;
         company.error = false;
         company.contracts = data.contracts;
         company.message = data.message;
      }).error(function(data) {
        company.status = true;
        company.error = true;
        company.message = data.message;
      });
  };
  
  this.contractCancel = function() {
    company.contract = {};
  };
 
}]);

})();
