(function () {
var app = angular.module('deleteFromList', []);

app.directive('confirmDelete', function() {
return {
    replace: true,
    templateUrl: '/js/angular/views/delete-confirmation.html',
    scope: {
      onConfirm: '&'
    },
    controller: function($scope) {
      $scope.isDeleting = false;
      
      $scope.startDelete = function() {
        $scope.isDeleting = true;
      };
      
      $scope.cancel = function() {
        $scope.isDeleting = false;
      };
      
      $scope.confirm = function() {
        $scope.onConfirm()
      };
      
    }
  }

});

})();