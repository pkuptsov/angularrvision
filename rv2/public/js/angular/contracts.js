(function(){
var app = angular.module('Contracts', []);

app.controller('ContractListController', ['$http', function ($http){
  var cntlist = this;
  
  cntlist.contracts= [ ];
  cntlist.newcontract = {};
  cntlist.status = false;
  cntlist.error = false;
  cntlist.page = 1;
  
  $http.get('/api/contracts.json').success(function(data) {
      cntlist.contracts = data;
  });

  this.contractDelete =  function(company_id, contract_id) {
  
      this.contract_id = contract_id || 0;
      this.company_id = company_id || 0;
      
      $http.post('/api/delete/contracts/' + contract_id + '/' + contract_id + '.json').success(function(data) {
         cntlist.status = true;
         cntlist.error = false;
         cntlist.contracts = data.contracts;
         cntlist.message = data.message;
      }).error(function(data) {
        cntlist.status = true;
        cntlist.error = true;
        cntlist.message = data.message;
      });
  };

  this.getPage = function(status) {
    // if newer page - increment
    if(status) {
      cntlist.page++;
    }// older page decrement if not 1
    else if(cntlist.page !== 1 ) {
      cntlist.page--;
    }
    else {
      cntlist.page = 1;
    }
    
    $http.get('/api/contracts.json?page=' + cntlist.page).success(function(data) {
      cntlist.contracts = data;
    });
  };

}]);

// edit contract
app.controller('ContractDetailController', ['$http', '$routeParams', '$location', function($http, $routeParams, $location) {
  var contract = this;
  contract.tab = 1;  
  contract.status = false;
  contract.banks = [];// list of accounts
  contract.contract = {};
  contract.error = false;
  contract.digitOnly =  /^\d+$/;
  
  $http.get('/api/banks/' + $routeParams.companyId + '.json').success(function(data) {
      contract.banks = data;
  });
    
  $http.get('/api/contracts/' + $routeParams.companyId + '/' + $routeParams.contractId + '.json').success(function(data) {
      contract.contract = data;
  });
  
  this.contractUpdate = function(contract) {
    this.contract = contract;
    
    $http.post('/api/update/contracts.json', contract).success(function(data) {
      contract.status = true;
      contract.error = false;
      contract.message = data.message;
    }).error(function(data) {
      contract.status = true;
      contract.error = true;
      contract.message = data.message;
    }); 
  };
  
  this.contractCancel = function() {
    $location.path('/companies/' + $routeParams.companyId);
  };
  
  this.selectTab = function(setTab) {
    contract.tab = setTab;
  };
  
  this.isSelected = function(checkTab) {
    return contract.tab === checkTab;
  };
  
   this.switchStatus = function() {
    contract.status = false;
  };
  
  this.contractAutoLong = function(auto_long) {
    this.auto_long = auto_long;
    if (auto_long === 1) {
        contract.contract.auto_long = 0;
    } else {
      contract.contract.auto_long = 1;
    }
    
  };
  
}]);


})();
