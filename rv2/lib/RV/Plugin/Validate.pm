package RV::Plugin::Validate;
use Mojo::Base 'Mojolicious::Plugin';
use common::sense;

my %VALIDATORS = (
                  login_form        => \&_validate_login_form,
                  company_form      => \&_validate_company_form,
                  );

sub register {
  my ($plugin, $app) = @_;

  # clean value (onl digit)
  $app->validator->add_filter( digit_only => sub {
                                 my ($vd, $name, $value) = @_;
                                 $value =~ s/[^0-9]+//g;
                                 return $value;
                                 }
                                 );
  
  $app->validator->add_filter( double_pg => sub {
                                 my ($vd, $name, $value) = @_;
                                 $value =~ s/,/./g;
                                 return $value;
                                 }
                                 );
  
  $app->helper( _validate_it => sub {
                  my ($self, $form) = @_;
                  my $v = $self->validation;
                  my $out = $VALIDATORS{$form}($v);
                  return $out;
                }
               );  
  }

###
sub _validate_login_form {
  my $validation = shift;
  $validation->required('email');
  $validation->required('password');
  say for @{$validation->failed};
  return $validation;
}

sub _validate_company_form {
  my $validation = shift;
}

1;
