package RV::Plugin::Helpers;
use base 'Mojolicious::Plugin';
use common::sense;
use Cache::Memcached;
#use Data::Dumper;
sub register {
my ($plugin, $app) = @_;

my $memd = Cache::Memcached->new($app->config->{memcached});

### Logging
$app->helper( logger  =>
    sub {
        my ($s, $email, $remote_ip, $failure) = @_;
        my %log;
        $log{'user_agent'} =  $s->req->headers->user_agent;#, 150);
        $log{'referrer'} =   $s->req->headers->referrer || 'direct';#, 255);
        $log{'email'} = $email;
        $log{'remote_ip'} = $remote_ip;
        $s->qinsert('login_log', {email => $log{'email'}, remote_ip => $log{'remote_ip'},
                               user_agent => $log{'user_agent'},referrer => $log{'referrer'}, failure => $failure});
        #say $s->qerror;
        return $log{'remote_ip'};
    }  
);

# formatted date
$app->helper( pretty_date  =>
    sub {
        my ($self, $date) = @_;
        return '' unless $date;
        $date =~ /(\d{4})\-(\d{2})-(\d{2})/;
        return "$3.$2.$1";
    }  
);

$app->helper( mc  => sub { return $memd });

}

1;
  
#########
