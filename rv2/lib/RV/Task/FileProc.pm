package RV::Task::FileProc;
use Mojo::Base 'Mojolicious::Plugin';
use common::sense;
use Mojo::Pg;
use Data::Dumper;
use Mojo::JSON qw(decode_json);
use Mojo::UserAgent;

use constant VIDEO_URL => 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=AIzaSyBn8yBc9nyA1vcO_gUeA8gxKXZn-ZpdkEk';

=head2
Плагин для работы с задачами
parse_file - парсим файл в БД, потом сделать отдельную задачу для окончательной обработки
=cut 
my $raw = Mojo::Pg->new('postgresql://rvision:Wwwrus2@localhost:5433/report_rvision');
$raw         = $raw->options({AutoCommit => 1, RaiseError => 1, PrintError => 1});


sub register {

  my ($self, $app) = @_;
  

  $app->minion->add_task(parse_file => sub {
    my ($job, $file_id) = @_;
    my $file_db = $job->app->qselect('batch_files', {id => $file_id});
    
    my $extract_directory = $file_db->{filedir} . "/" . $file_id;
    
    eval {
        system("/usr/bin/unzip -o " .  $file_db->{filedir} . '/' . $file_db->{filename} . " -d $extract_directory");
    };

    if($@) {
        # сбрасываем статус в таблице
        $job->app->qupdate('batch_files', {id => $file_id}, {status => 'new'});
        return;
    }

    $job->app->qupdate('batch_files', {id => $file_id}, {status => 'init'});
    
    # удаляем zip файл
    unlink($file_db->{filedir} . '/' . $file_db->{filename});
    
    opendir(DIR, $extract_directory) 
        || do { 
                $job->app->log->debug("Can't open directory $extract_directory::$!"); 
                return;
              };
    
    my ( $unzipped_file ) = map { "$extract_directory/$_" } 
                            grep { $_ !~ /^\./ && 
                                  -f "$extract_directory/$_" 
                                 }
                            readdir(DIR);

    unless(-e $unzipped_file) {
        $job->app->log->debug('Not found:' . $unzipped_file);
        return;
    }
    
    unless ( _validate_file($unzipped_file) ) {
        $job->app->log->debug("can't pass validation");
        $job->app->qupdate('batch_files', 
                           { id => $file_id}, 
                           { status => 'format_error' } 
                          );
        return;
    }
    
    # если файл есть - продолжаем обрабатывать
    my $tmp_table = _create_table_movie_pre($file_id);
    
    eval {
        $raw->db->query("COPY $tmp_table FROM '$unzipped_file' DELIMITER ',' CSV HEADER;");
    } 
    || 
    do {
        $job->app->log->debug("Error during file import to db vie COPY::$@");
        
        $job->app->qupdate('batch_files', 
                   { id => $file_id}, 
                   { status => 'format_error' } 
                  );
        eval {
            unlink($unzipped_file);
            rmdir($extract_directory);
        } || 
        do { $job->app->log->debug("Error during deleting $unzipped_file and $extract_directory $@"); };
        return;
    };
    
    # удаляем файл после обработки
    eval {
      unlink($unzipped_file);
      rmdir($extract_directory);
    } || 
    do { $job->app->log->debug("Error during deleting $unzipped_file and $extract_directory $@"); };
    
    # 
     $job->app->qupdate('batch_files', {id => $file_id}, {status => 'pars'});
     $raw->db->query(qq{
         INSERT INTO movies (
                "Video_ID", 
                "Custom_ID", 
                "Day",
                "Owned_Views",
                "Owned_Views_Watch_Page",
                "Owned_Views_Embedded_Player",
                "Owned_Views_Channel_Page",
                "Owned_Views_Live",
                "Owned_Views_On_Demand",
                "Owned_Views_Ad_Enabled", 
                "Owned_Views_Ad_Requested",
                "YouTube_Revenue_Split_AdSense_Served_YouTube_Sold",
                "YouTube_Revenue_Split_DoubleClick_Served_YouTube_Sold",
                "YouTube_Revenue_Split_DoubleClick_Served_Partner_Sold",
                "YouTube_Revenue_Split_Partner_Served_Partner_Sold",
                "YouTube_Revenue_Split",
                "Partner_Revenue_AdSense_Served_YouTube_Sold",
                "Partner_Revenue_DoubleClick_Served_YouTube_Sold",
                "Partner_Revenue_DoubleClick_Served_Partner_Sold",
                "Partner_Revenue_Partner_Served_Partner_Sold",
                "Partner_Revenue",
                "file_id"
                )
        SELECT mp."Video ID", mp."Custom ID", mp."Day", 
               SUM(mp."Owned Views"),
               SUM(mp."Owned Views : Watch Page"),
               SUM(mp."Owned Views : Embedded Player"),
               SUM(mp."Owned Views : Channel Page"),
               SUM(mp."Owned Views : Live"),
               SUM(mp."Owned Views : On Demand"),
               SUM(mp."Owned Views : Ad-Enabled"), 
               SUM(mp."Owned Views : Ad-Requested"),
               SUM(mp."YouTube Revenue Split : AdSense Served YouTube Sold"),
               SUM(mp."YouTube Revenue Split : DoubleClick Served YouTube Sold"),
               SUM(mp."YouTube Revenue Split : DoubleClick Served Partner Sold"),
               SUM(mp."YouTube Revenue Split : Partner Served Partner Sold"),
               SUM(mp."YouTube Revenue Split"),
               SUM(mp."Partner Revenue : AdSense Served YouTube Sold"),
               SUM(mp."Partner Revenue : DoubleClick Served YouTube Sold"),
               SUM(mp."Partner Revenue : DoubleClick Served Partner Sold"),
               SUM(mp."Partner Revenue : Partner Served Partner Sold"),
               SUM(mp."Partner Revenue"),
               ?::int as file_id
          FROM $tmp_table mp 
         WHERE mp."Policy"='monetize'
           AND NOT EXISTS ( SELECT * 
                              FROM movies mv
                             WHERE mv."Video_ID"  = mp."Video ID" 
                               AND mv."Custom_ID" = mp."Custom ID" 
                               AND mv."Day"       = mp."Day" 
                          ) 
        GROUP BY "Video ID","Custom ID", "Day", file_id},
    $file_id
    );
    # 
     $job->app->qupdate('batch_files', {id => $file_id}, {status => 'grup'});
     # удаляем из временной таблицы данные
     $raw->db->query("DROP TABLE $tmp_table");
     #$job->app->qdelete('movie_pre', {file_id => $file_id});


  });

 $app->minion->add_task(api_req => sub {
    my ($job, $file_id) = @_;
    #say 'Run file id: ' . $file_id;
    # add headers
    my $hdr = {
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
                'Referer'    => 'http://hostx.ru/',
    }; 
    # то что есть в импорте
    my $movies = $raw->db->query(qq{SELECT distinct "Video_ID" FROM movies WHERE file_id = ?}, $file_id);
    
    #my $cv = AnyEvent->condvar;
    my $ua = Mojo::UserAgent->new();

    while(my $next = $movies->hash){
        # проверяем есть ли это видео в movies_list
        # выборка существующих в БД произведений
        my $is_movie_exists = $job->app->qcount('movies_list',{video_id => $next->{Video_ID}});
        next if $is_movie_exists; # если есть уже - пропускаем
        my $url = sprintf(VIDEO_URL, $next->{Video_ID});
        my $res = $ua->get($url => $hdr)->res;    
        my $res_json = $res->json;
        
        unless($res_json->{items}[0]->{snippet}->{title} &&
           $res_json->{items}[0]->{snippet}->{channelTitle})
        {
            warn "Error on fetching url: $url content => $_[0]";
            
            $raw->db->query(qq{UPDATE movies 
                                  SET "errors" = coalesce(errors, '[]')::jsonb || coalesce(?, '"unknown error"')::jsonb 
                                WHERE "Video_ID" = ?
                               },
                            $res->body,
                            $next->{Video_ID}
                            );   
            
            next;
        }
        warn "\n\n save Video name for $next->{Video_ID} \n\n";
            $raw->db->query(qq{UPDATE movies 
                          SET "Video_Name" = ?, 
                              channelname = ?
                        WHERE "Video_ID" = ?},
                    $res_json->{items}[0]->{snippet}->{title}, 
                    $res_json->{items}[0]->{snippet}->{channelTitle}, 
                    $next->{Video_ID}
                    );
  # пока остается избыточность данных в 2-ъ таблицах
  # вносим те видео которых еще нет в таблице movies_list
                    $raw->db->query(qq{INSERT INTO movies_list (video_id,video_name,channel_name)
                                        VALUES(?,?,?)},
                    $next->{Video_ID},
                    $res_json->{items}[0]->{snippet}->{title}, 
                    $res_json->{items}[0]->{snippet}->{channelTitle}
                    );

    }

    my ($count) = $raw->db->query(qq{SELECT count(*) 
                                        FROM movies 
                                       WHERE file_id = ? 
                                         AND "Video_ID" is not null 
                                         AND channelname is not null 
                                       },
                                    $file_id
                                  );
    
    if ($count){
        $raw->db->query("UPDATE batch_files
                            SET status = 'done'
                          WHERE id = ?",
                        $file_id);
    }
    $raw->db->query( qq{REFRESH MATERIALIZED VIEW mv_movies} );   
    
  });

}

sub _create_table_movie_pre {
    my ($file_id) = @_;
    
my $sql = <<SQL;
CREATE TABLE public.movie_pre_${file_id}
(
  "Video ID" character varying(11),
  "Custom ID" character varying(20),
  "Day" date,
  "Country" character varying(2),
  "Content Type" character varying(20),
  "Policy" character varying(10),
  "Owned Views" integer,
  "Owned Views : Watch Page" integer,
  "Owned Views : Embedded Player" integer,
  "Owned Views : Channel Page" integer,
  "Owned Views : Live" integer,
  "Owned Views : On Demand" integer,
  "Owned Views : Ad-Enabled" integer, 
  "Owned Views : Ad-Requested" integer,
  "YouTube Revenue Split : AdSense Served YouTube Sold" double precision,
  "YouTube Revenue Split : DoubleClick Served YouTube Sold" double precision,
  "YouTube Revenue Split : DoubleClick Served Partner Sold" double precision,
  "YouTube Revenue Split : Partner Served Partner Sold" double precision,
  "YouTube Revenue Split" double precision,
  "Partner Revenue : AdSense Served YouTube Sold" double precision,
  "Partner Revenue : DoubleClick Served YouTube Sold" double precision,
  "Partner Revenue : DoubleClick Served Partner Sold" double precision,
  "Partner Revenue : Partner Served Partner Sold" double precision,
  "Partner Revenue" double precision
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.movie_pre_${file_id}
  OWNER TO rvision;

CREATE INDEX movie_pre_${file_id}_ix
  ON public.movie_pre
  USING btree
  ("Video_ID" COLLATE pg_catalog."default", "Custom_ID" COLLATE pg_catalog."default", "Day", "Policy" COLLATE pg_catalog."default");
SQL

$raw->db->query($sql);

return "movie_pre_${file_id}";

}

sub _validate_file {
    my ($file) = @_;

    open FILE, $file or return;
    
    chomp(my $header = <FILE>);
    chomp(my $line   = <FILE>);
    
    close FILE;

    return _check_header( $header ) &&
           _check_line( $line )
           ? 1 : 0;
}

sub _check_header {
    my ($header) = @_;

    my @header_array =  (
        'Video ID',
        'Custom ID',
        'Day',
        'Country',
        'Content Type',
        'Policy',
        'Owned Views',
        'Owned Views : Watch Page',
        'Owned Views : Embedded Player',
        'Owned Views : Channel Page',
        'Owned Views : Live',
        'Owned Views : On Demand',
        'Owned Views : Ad-Enabled',
        'Owned Views : Ad-Requested',
        'YouTube Revenue Split : AdSense Served YouTube Sold',
        'YouTube Revenue Split : DoubleClick Served YouTube Sold',
        'YouTube Revenue Split : DoubleClick Served Partner Sold',
        'YouTube Revenue Split : Partner Served Partner Sold',
        'YouTube Revenue Split',
        'Partner Revenue : AdSense Served YouTube Sold',
        'Partner Revenue : DoubleClick Served YouTube Sold',
        'Partner Revenue : DoubleClick Served Partner Sold',
        'Partner Revenue : Partner Served Partner Sold',
        'Partner Revenue'
    );
    return $header eq join(",", @header_array) ? 1 : 0;
}

sub _check_line {
    my ($line) = @_;

    return 
    $line =~ 
        /^
          .{11},    # "Video ID" character varying(11), 
          .{0,20},  # "Custom ID" character varying(20),
          \d{8},    # "Day" date,
          [A-Z]{2}, # "Country" character varying(2),
          .{0,20},  # "Content Type" character varying(20),
          .{0,10},  # "Policy" character varying(20),
          \d+,      # "Owned Views" integer,
          \d+,      # "Owned Views : Watch Page" integer,
          \d+,      # "Owned Views : Embedded Player" integer,
          \d+,      # "Owned Views : Channel Page" integer,
          \d+,      # "Owned Views : Live" integer,
          \d+,      # "Owned Views : On Demand" integer,
          \d+,      # "Owned Views : Ad-Enabled" integer, 
          \d+,      # "Owned Views : Ad-Requested" integer,
          [0-9\.]+, # "YouTube Revenue Split : AdSense Served YouTube Sold" double precision,
          [0-9\.]+, # "YouTube Revenue Split : DoubleClick Served YouTube Sold" double precision,
          [0-9\.]+, # "YouTube Revenue Split : Partner Served Partner Sold" double precision,
          [0-9\.]+, # "Partner Revenue : AdSense Served YouTube Sold" double precision,
          [0-9\.]+, # "Partner Revenue : DoubleClick Served YouTube Sold" double precision,
          [0-9\.]+, # "Partner Revenue : DoubleClick Served Partner Sold" double precision,
          [0-9\.]+, # "Partner Revenue : Partner Served Partner Sold" double precision,
          [0-9\.]+  # "Partner Revenue" double precision
          $
         /x   
     ? 1: 0;
}

1;
