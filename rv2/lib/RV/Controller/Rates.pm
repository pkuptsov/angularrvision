package RV::Controller::Rates;
use Mojo::Base 'Mojolicious::Controller';
use POSIX qw(ceil);

sub get_rates {
  my $s = shift;
  my $page_id = $s->param('page_id') || 1;
  my $per_page = $s->app->config->{per_page};
  $page_id--;
   
  my $pages = $s->qcount('rates');
  my ($rates) = $s->qselect('rates',{},{ order_by => { desc => 'date' },
                                         limit => $per_page,
                                         offset => $page_id * $per_page });
  
  # определяем кол-во страниц на страницу :)
  my $count = ceil($pages/$s->app->config->{per_page});
  # если кол-во больше 1 - генерим список
  $pages = [1..$count] if $count > 1;

  $s->render(json => { list => $rates, pages => $pages });  
  
}

1;


