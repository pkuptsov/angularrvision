package RV::Controller::Users;
use Mojo::Base 'Mojolicious::Controller';

use Digest::MD5 qw(md5_hex);
use Encode qw(encode_utf8);

# This action will render a template
sub welcome {
  my $s = shift;
  $s->res->headers->cache_control('private, max-age=0, no-cache');
  $s->render('main/welcome');
}


sub get_users {
  my $s = shift;
  my $users = $s->qcustom(qq{
                            SELECT a.*, b.name
                            FROM users a LEFT JOIN companies b ON a.company_id=b.id
                            });
  $users = $users->hashes->to_array;
  $s->render(json => $users);  
}

sub save_user {
  my $s = shift;
  my $params = $s->req->json;
  my $id = $s->qinsert('users', {email => $params->{email}, password => md5_hex(encode_utf8($params->{password})),
                                 firstname =>  $params->{firstname}, lastname =>  $params->{lastname},
                                 surname =>  $params->{surname}, role => $params->{role},
                                 allow_ip => $params->{allow_ip}, company_id => $params->{company_id} });
  
  if ($s->qerror) {
  $s->render(status => 403,
             json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Добавлен успешно', id => $id});  
  }
  
}

sub delete_user {
    my $s = shift;
    my $user_id = $s->param('user_id');
    $s->qdelete('users',{id => $user_id});
    
    if ($s->qerror) {
    $s->render(
               status => 403, 
               json => {message => 'Ошибка: ' . $s->qerror}
               );
    }
    else {
    my ($users) = $s->qselect('users');
    $s->render(json => { userlist => $users, message => 'Удален успешно'});
    }
}

sub update_user {
  my $s = shift;
  my $params = $s->req->json;
  # если длина пароля ровно 32 символа -значит md5
  my $md5 = (length($params->{password}) == 32) ? 'true' : 0;
  # пришел пароль в md5
  if ($md5) {
    $s->qupdate('users', {id => $params->{id}}, { email => $params->{email},
                                      firstname =>  $params->{firstname}, lastname =>  $params->{lastname},
                                      surname =>  $params->{surname}, role => $params->{role},
                                      allow_ip => $params->{allow_ip}, company_id => $params->{company_id} });
  } # пришел обновленный пароль
  else {
    $s->qupdate('users', {id => $params->{id}}, { email => $params->{email}, password => md5_hex(encode_utf8($params->{password})),
                                      firstname =>  $params->{firstname}, lastname =>  $params->{lastname},
                                      surname =>  $params->{surname}, role => $params->{role},
                                      allow_ip => $params->{allow_ip}, company_id => $params->{company_id} });
  }
  
  if ($s->qerror) {
  $s->render(status => 403, 
             json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Обновлено успешно!'});
  }
}

1;
