package RV::Controller::Contracts;
use Mojo::Base 'Mojolicious::Controller';

sub get_contracts {
    my $s = shift;
    my $contract_id = $s->param('contract_id');
    my $company_id  = $s->param('company_id');

    my $r = $s->_get_contracts($s->param('company_id'), 
                               $s->param('contract_id'), 
                               $s->param('page'), 
                               $s->param('rows'),
                               );
                         
    $r = $r->[0] if $contract_id;
    
    $s->render( json => $r );
}

sub _get_contracts {
    my ($s, $company_id, $contract_id, $page, $rows) = @_;
    
    $page ||= 1;
    $rows ||= 50;

    my $request = "SELECT cm.name as company_name ,cm.id as company_id, 
                          cn.id as id, cn.name as name, cn.date_sign as date_sign, 
                          cn.date_end as date_end, cn.sign_fio as sign_fio,
                          cn.bank_id as bank_id, cn.auto_long as auto_long 
                     FROM public.contracts cn, public.companies cm 
                    WHERE cm.id = cn.company_id";
                    
    $request .= " AND cm.id = $company_id" if $company_id;
    $request .= " AND cn.id = $contract_id" if $contract_id;
    
    $request .= sprintf(" ORDER BY cn.id DESC LIMIT %d OFFSET %d", $rows, ( $page - 1 ) * $rows );

    my $contracts = $s->qcustom($request);
    say $s->dumper($@) if $s->qerror;

    return $contracts ? $contracts->hashes->to_array : [];
}

sub save_contract {
  my $s = shift;
  my $params = $s->req->json;
  my $id = $s->qinsert('contracts', { name => $params->{name}, company_id => $params->{company_id},
                                 bank_id =>  $params->{bank_id}, date_sign =>  $params->{date_sign},
                                 date_end =>  $params->{date_end}, auto_long =>  $params->{auto_long},
                                 sign_position =>  $params->{sign_position}, sign_fio =>  $params->{sign_fio}
                                });
  
  if ($s->qerror) {
  $s->render(status => 403, json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Добавлен успешно', id => $id});  
  }
  
}
#
sub update_contract {
  my $s = shift;
  my $params = $s->req->json;
  
  $s->qupdate('contracts', {id =>  $params->{id}}, { name => $params->{name}, company_id => $params->{company_id},
                                 bank_id =>  $params->{bank_id}, date_sign =>  $params->{date_sign},
                                 date_end =>  $params->{date_end}, auto_long =>  $params->{auto_long},
                                 sign_position =>  $params->{sign_position}, sign_fio =>  $params->{sign_fio}
                                });
  
  if ($s->qerror) {
  $s->render(status => 403, json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Обновлен успешно', id => $params->{id}});  
  }
  
}

sub delete_contract {
    my $s = shift;
    my $contract_id = $s->param('contract_id');
    my $company_id = $s->param('company_id');
    $s->qdelete('contracts',{id => $contract_id});
    
    if ($s->qerror) {
      $s->render(
              status => 403, 
              json => {message => 'Ошибка: ' . $s->qerror}
              );
    }
    else {
      $s->render(json => { contracts => $s->_get_contracts(), message => 'Удален успешно'});
    }
}

1;
