package RV::Controller::Import;
use Mojo::Base 'Mojolicious::Controller';
use FindBin qw($Bin);
use POSIX qw(ceil);

$ENV{MOJO_MAX_MESSAGE_SIZE} = 40971520;# 40mb



sub save_file {
  my $c = shift;
  my $file = $c->param('data_file');
  my $filename = time() . '_' .  $file->filename;
  my $filedir = $Bin . '/..' . $c->config('tmp');
  $file->move_to(qq{$filedir/$filename});
  my $batch_id = $c->qinsert('batch_files', {filename => $filename, status => 'new',
                                             filedir  => $filedir});

  $c->render(json => {batch => $batch_id});  
}


sub list_files {
  my $c = shift;
  my $filelist = $c->qcustom(qq{SELECT   id, filename, status, to_char(date,'DD.MM.YYYY HH:MM') as date, filedir
                                FROM batch_files});
  $c->render(json => $filelist->hashes->to_array);
}

sub delete_file {
  my $c = shift;
  my $file_id = $c->param('file_id');
  my $file_db = $c->qselect('batch_files',{id => $file_id});
  my $file_name = $file_db->{filedir} . '/' . $file_db->{filename};
  
  eval {
  unlink($file_name) if (-e $file_name);
  };
  
  if ($@) {
    $c->render(status => 404, json => {status => $@});
  }
  else {
    $c->qdelete('batch_files',{id => $file_id});
    $c->render(json => {status => 'ok'});
  }
  
}

=head2
process_file - получаем имя файла, пишем в БД в таблицу batch_files,
добавляем задачу на миньон для обработки файла
=cut

sub process_file {
  my $c = shift;
  my $file_id = $c->param('file_id');
  $c->qupdate('batch_files', {id => $file_id}, {status => 'work'});
  # ставим в очередь задачу по обработке файла
  $c->minion->enqueue(parse_file => [$file_id] );
  my ($filelist) = $c->qselect('batch_files');
  $c->render(json => $filelist);
}

=head2
    progress_file - проверка готовности обработки файла
=cut
sub progress_file {
    my $c = shift;
    my $file_id = $c->param('file_id');

    my $movies = $c->qcustom(qq{ SELECT 
                                (SELECT count( distinct m."Video_ID" ) 
                                   FROM movies m
                                  WHERE m.file_id = ?
                                ) AS total_count,
                                (SELECT count( distinct mm."Video_ID" ) 
                                   FROM movies mm
                                  WHERE mm.file_id = ?
                                    AND (mm."Video_Name" IS NOT NULL OR 
                                         mm."errors" IS NOT NULL
                                        )
                                ) AS processed_count,
                                ( SELECT b.status
                                    FROM batch_files b
                                   WHERE b.id = ?
                                ) AS status
                               },
                               $file_id, 
                               $file_id,
                               $file_id
                              );
                              
     say $c->dumper($@) if $c->qerror;
     
     return $c->render(json => $movies->hash);
}

sub api_req {
  my $c = shift;
  my $file_id = $c->param('file_id');
  $c->qupdate('batch_files', {id => $file_id}, {status => 'api'});
  # ставим в очередь задачу по обработке файла
  $c->minion->enqueue(api_req => [$file_id] );
  my ($filelist) = $c->qselect('batch_files');
  $c->render(json => $filelist);
}

sub movies {
  my $c = shift;
  
  my $items_in_page_cnt = $c->app->config->{per_page};
  my $p                 = $c->param('page_id') || 1;
  my $search            = $c->param('search')  || "";
  my $order             = $c->param('order')   || "";
  my $order_direction   = $c->param('order_direction') || "";
  
  $order           = "Video_Name" unless $order =~ /^(?:Video_Name|Video_ID|channelname|Total_Views)$/;
  $order_direction = "asc" unless $order_direction =~ /^(?:asc|desc)$/;

  $search =~ s/[;"']//g;

  my $total_items_count = $c->qcustom(qq{SELECT count("Video_Name")  as count
                                           FROM mv_movies
                                          WHERE ?::text is null OR "Video_Name" ~* ?::text 
                                         },
                          $search || undef,
                          $search || undef
                         )->hash->{count};
                         
  my $page_count = ceil( $total_items_count / $items_in_page_cnt );
  
  my $movies = $c->qcustom(qq{SELECT "Video_Name", "Video_ID", "channelname", "Total_Views" 
                                FROM mv_movies 
                               WHERE ?::text is null OR "Video_Name" ~* ?::text 
                            ORDER BY "$order" $order_direction
                               LIMIT $items_in_page_cnt 
                             OFFSET ( $p - 1 ) * $items_in_page_cnt
                                },
                                $search || undef,
                                $search || undef
                                );
                                  
  say $c->dumper($@) if $c->qerror;
  $movies = $movies->hashes->to_array;

  $c->render(json => { movies => $movies, 
                        pages => [1..$page_count],
                        total => $total_items_count, 
                     }
                    );
}

# нужно кэшировать
sub statistic {
  my $c = shift;
  my $file_id = $c->param('file_id');
  return $c->render(json => {total => 0}) unless $file_id;
  my $total = $c->qcustom(qq{WITH video_count(uniq) AS (SELECT 1 FROM movies WHERE file_id = ?
                             GROUP BY "Video_ID")
                            SELECT SUM(uniq) FROM video_count}, $file_id)->hash;
  #say $c->dumper($total);
  my $ready = $c->qcustom(qq{WITH video_count(uniq) AS (SELECT 1 FROM public.movies  WHERE movies.file_id= ?
                             AND movies."Video_Name" is not null
                             GROUP BY "Video_ID" )
                             SELECT SUM(uniq) FROM video_count}, $file_id)->hash;
  my $errors = $total->{sum} - $ready->{sum};
  $c->render(json => { total => $total->{sum},
                       ready => $ready->{sum},
                       errors => $errors
                      });
}
1;
