package RV::Controller::Reports;
use Mojo::Base 'Mojolicious::Controller';
 
sub report1 {
  my $c = shift;
  my $vars = $c->req->json;
  my $crypt = $c->session('crypt');
  #say "==== CRYPT KEY ===";
  #say $crypt;
  my $memc = $c->mc;
  my $contracts = $c->qcustom(qq{SELECT a.name as company, b.name as contract, b.date_sign
                                 FROM companies a
                                 INNER JOIN contracts b
                                 ON a.id = b.company_id
                                 WHERE b.id= ? },$vars->{id});
  
  my $movies =  $c->qcustom(qq{SELECT video_id, video_name
                            FROM movies_list
                            WHERE object_id IN(SELECT id FROM movie_objects WHERE contract_id = ?)}, $vars->{id});

  my $results = [];
  my $total = {views => 0, sum => 0};
  while (my $next = $movies->array)
  {
    my $video = $c->qcustom(qq{SELECT SUM("Owned_Views") AS viewes,
                               SUM("YouTube_Revenue_Split_AdSense_Served_YouTube_Sold") AS total
                               FROM movies
                               WHERE  "Video_ID" = ? AND "Day" BETWEEN ? AND ?},$next->[0],$vars->{from_date},$vars->{to_date});
    my $hash = $video->hash;
    push(@$results,{video_id => $next->[0], video_name => $next->[1],
                    viewes => $hash->{viewes}, total => $hash->{total} });
    $total->{views} += $hash->{viewes};
    $total->{sum} += $hash->{total};
  }
  # $crypt - ключ в сессии - используем как доп.идентификатор
  $memc->set('to_pdf_data_'.$crypt,
             { header => $contracts->hash,
               body   => $results,
               footer => $total,
               vars   => $vars
             });

  my $status = [];
  if ($^O eq 'MSWin32') {
    $status = [0,0];
  }
  else
  {
  my $cmd = "/usr/bin/wkhtmltopdf.sh --cookie crypt $crypt --ignore-load-errors https://report.rvision.tv/api/reports/format_pdf/r3v1z/";
  $status->[0] = system($cmd . '1.html ' . $c->app->config->{'pdf_path'} . $crypt . '_report1.pdf');
  $status->[1] = system($cmd . '2.html ' . $c->app->config->{'pdf_path'} . $crypt . '_report2.pdf');
  }
  $c->render(json => {list => $results , total => $total, status => $status});
}

# Этот роут защищен только паролем и так же
# ему нужно получать crypt ключ для получения из мемкеша данных
# crypt ключ здесь получается из куки - т.к. этот ключ передает wkhtmltopdf утилита
sub format_pdf {
  my $c = shift;
  my $memc = $c->mc;
  my $id = $c->param('id');
  my $pass = $c->param('pass');
  my $crypt = $c->cookie('crypt') || 'none';
  if ($pass ne $c->app->config->{'restrict'} and $crypt eq 'none')
  {
    return $c->redirect_to('/login');
  }
  
  my $to_pdf_data = $memc->get('to_pdf_data_'.$crypt);
  
  return $c->redirect_to('/login') unless $to_pdf_data;
  
  $id =~ s/[^1-2]//;
  $id = $id || 1;

  $c->stash(
            movies => $to_pdf_data->{body},
            total  => $to_pdf_data->{footer},
            header => $to_pdf_data->{header},
            vars   => $to_pdf_data->{vars},
            );
  $c->render('export_to_pdf/template' . $id);  
}

sub download_pdf {
  my $c = shift;
  my $file = $c->param('file');
  my $crypt = $c->session('crypt');
  
  return $c->redirect_to('/login') unless $crypt;
  
  my $relfile = $c->app->config->{'pdf_path'} . $crypt . '_' . $file . '.pdf';
  
  if( -e $relfile)
  {
    $c->render_file(
                  'filepath' => $relfile,
                  'format'   => 'pdf',
                  'content_disposition' => 'inline',
                  'cleanup'  => 1,     
                  'filename' => $file . '.pdf');    
  }
  else
  {
    $c->render(text => 'Файл не найден!') ;
  }

}

1;
