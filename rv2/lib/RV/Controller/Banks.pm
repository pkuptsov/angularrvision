package RV::Controller::Banks;
use Mojo::Base 'Mojolicious::Controller';

sub get_banks {
  my $s = shift;
  my $bank_id = $s->param('bank_id');
  my $company_id = $s->param('company_id');
  if ($bank_id) { # TODO add company_id to select
  my $bank = $s->qselect('accounts',{id => $bank_id});
  $s->render(json => $bank);
  }
  else {
  my ($banks) = $s->qselect('accounts',{company_id => $company_id});
  $s->render(json => $banks);  
  }
  
}

sub save_bank {
  my $s = shift;
  my $params = $s->req->json;
  my $id = $s->qinsert('accounts', $params);
  
  if ($s->qerror) {
  $s->render(status => 403,
             json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Добавлен успешно', id => $id});  
  }
  
}

sub update_bank {
  my $s = shift;
  my $params = $s->req->json;
  $s->qupdate('accounts', {id =>  $params->{id}},  { name => $params->{name}, company_id => $params->{company_id},
                                 bik =>  $params->{bik}, kor =>  $params->{kor},
                                 rasch =>  $params->{rasch},
                                });
  
  if ($s->qerror) {
  $s->render(status => 403,
             json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Обновлен успешно', id => $params->{id}});  
  }
  
}

sub delete_bank {
  my $s = shift;
  my $bank_id = $s->param('bank_id');
  my $company_id = $s->param('company_id');
  $s->qdelete('accounts',{id => $bank_id});

  if ($s->qerror) {
    $s->render(
             status => 403, 
             json => {message => 'Ошибка: ' . $s->qerror}
             );
  }
  else {
    my ($banks) = $s->qselect('accounts',{ company_id => $company_id });
    $s->render(json => { banks => $banks, message => 'Удален успешно'});
  }
}

1;
