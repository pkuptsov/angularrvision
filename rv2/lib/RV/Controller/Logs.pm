package RV::Controller::Logs;
use Mojo::Base 'Mojolicious::Controller';
use POSIX qw(ceil);

sub get_logs {
    my $s = shift;
    my $page_id = $s->param('page_id');
    $page_id = 1 unless $page_id && $page_id =~ /^\d+$/;
    $page_id--;
    
    my $where = qq{(a."time")::date = (now() - '$page_id day'::interval)::date};

    my $search = $s->param('search');
    $search = undef if !$search || $search =~ /[;'"]/;

    $where = qq{(a."remote_ip")::text   ~ '$search' OR
                a."email"               ~ '$search' OR
                a."user_agent"          ~ '$search' OR
                (a."time")::text        ~ '$search'
               } if $search;
    
    my $logs = $s->qcustom(qq{SELECT to_char(a."time", 'HH24:MM DD-MM-YYYY') AS time_line, a.email, a.remote_ip,
                                     a.user_agent as trim_useragent,
                                     a.failure
                                FROM login_log a 
                               WHERE $where
                            ORDER BY a."time" DESC 
                          });
    
    $where = "1=1" unless $search;
    my $last_page = $s->qcustom(qq{SELECT extract('day' from (now()-min(a.time))::interval) as last_page
                                      FROM login_log a
                                     WHERE $where 
                                 })->hash->{last_page};
                                 
    $s->render(json => $logs 
                       ? { logs  => $logs->hashes->to_array,
                           pages => [1..$last_page]
                         }
                       : {
                           logs =>  [],
                           pages => [1]
                         }
    );

}

sub movies_error_log {
    my $c = shift;
    
    my $items_in_page_cnt = $c->app->config->{per_page};
    my $p                 = $c->param('page_id') || 1;
    my $file_id = $c->param('file_id');
    my $search  = $c->param('search');
    $search =~ s/;//g;
    
    my $where = "errors is not null";
    $where .= " AND file_id = '$file_id' " if $file_id && $file_id =~ /^\d+$/;
    $where .= qq{ AND ( file_id::text ~ '$search' OR "Video_ID"::text ~* '$search' OR errors::text ~* '$search' )}
                if $search;
                
    my $total = $c->qcustom(qq{SELECT count("Video_ID") as count
                                FROM movies
                               WHERE $where});
                               
    my $total_items = $total->hash->{count} if $total;
    my $page_cnt = ceil( $total_items / $items_in_page_cnt );
    
    my $logs = $c->qcustom(qq{SELECT "Video_ID", file_id, errors
                                FROM movies
                               WHERE $where 
                            ORDER BY file_id desc
                               LIMIT $items_in_page_cnt 
                             OFFSET ( $p - 1 ) * $items_in_page_cnt} 
                          );
                          
   $c->render(json => $logs 
                        ? { logs  => $logs->hashes->to_array,
                           pages => [1..$page_cnt]
                          }
                        : {
                           logs =>  [],
                           pages => [1]
                          }
             );
}

1;
