package RV::Controller::Companies;
use Mojo::Base 'Mojolicious::Controller';

sub get_companies {
  my $s = shift;
  my $company_id = $s->param('company_id');
  if ($company_id) {
  my $company = $s->qselect('companies',{id => $company_id});
  $s->render(json => $company);
  }
  else {
  my ($companies) = $s->qselect('companies');
  $s->render(json => $companies);  
  }
  
}

sub save_company {
  my $s = shift;
  my $params = $s->req->json;
  my $id = $s->qinsert('companies', { name => $params->{name}, inn => $params->{inn},
                                 kpp =>  $params->{kpp}, tel =>  $params->{tel},
                                 contract_position =>  $params->{contract_position},
                                 contract_sign => $params->{contract_sign}, boss => $params->{boss} });

  if ($s->qerror) {
  $s->render(status => 403,
             json => {message => 'Ошибка: ' . $s->qerror});
  say $@;
  }
  else {
  $s->render(json => {message => 'Добавлена успешно', id => $id});  
  }
  
}

sub update_company {
  my $s = shift;
  my $params = $s->req->json;
  $s->qupdate('companies', {id => $params->{id}},  { name => $params->{name}, inn => $params->{inn},
                                           kpp =>  $params->{kpp}, tel =>  $params->{tel},
                                           contract_position =>  $params->{contract_position},
                                           contract_sign => $params->{contract_sign}, boss => $params->{boss} });
  
  if ($s->qerror) {
  $s->render(status => 403,
             json => {message => 'Ошибка: ' . $s->qerror});
  }
  else {
  $s->render(json => {message => 'Обновлено успешно!'});
  }
  
}

sub delete_company {
   my $s = shift;
   my $company_id = $s->param('company_id');
   $s->qdelete('companies',{id => $company_id});
   
   if ($s->qerror) {
    $s->render(
               status => 403, 
               json => {message => 'Ошибка: ' . $s->qerror}
               );
   }
   else {
    my ($companies) = $s->qselect('companies');
    $s->render(json => { companies => $companies, message => 'Удален успешно'});
   }

}

1;
