package RV::Controller::Login;
use Mojo::Base 'Mojolicious::Controller';
use Digest::MD5 qw(md5_hex);
use Encode qw(encode_utf8);

sub login {
  shift->render('forms/login');
}


sub signin {
  my $s = shift;
  
  my $validation = $s->_validate_it('login_form');
  
  return $s->redirect_to('/') if $validation->has_error;
  
  my $remember = $s->param('remember') || 0;
  
  if ($remember) {
    $s->session(expiration  => 3600*24*30);# 1 month exp
  }
  else {
    $s->session(expires => time + 180);
  }
  
  my $vars = $validation->output;
  my $data = $s->qselect('users',{email => $vars->{email}, password => md5_hex(encode_utf8($vars->{password}))});
  my $remote_ip = $s->req->headers->header('X-Real-IP')
               || $s->req->headers->header('X-Forwarded-For')
               || $s->tx->original_remote_address
               ||  $s->tx->remote_address;
  
  if ( $data->{role} and  ( $data->{allow_ip} eq $remote_ip  or $data->{allow_ip} eq '127.0.0.1') ) {
    $s->logger($vars->{email}, $remote_ip, 'false');
    $s->session(
                role  => $data->{role},
                email => $data->{email},
                id    => $data->{id},
                crypt => md5_hex($data->{role} . $data->{email} .$data->{id} . $s->app->config->{'restrict'}),
                );  
    $s->redirect_to('/'); 
  }
   else {
    $s->logger( $vars->{email}, $remote_ip, 'true' );
    $s->redirect_to('/notice/loginerror');  
  }
  
}

sub logout {
  my $s = shift;
  $s->session(expires => 1);
  $s->redirect_to('/login');
}

sub error {
  my $s = shift;
  my $error = $s->param('error');
  my %errors = (
                loginerror => 'Access forbiden!',
                inputerror => 'Invalid parameters!'
                );
  $s->stash(
            title => 'Ошибка',
            type => 'danger',
            msg  => $errors{$error}
            );
  $s->render('notice/error');
}

1;
