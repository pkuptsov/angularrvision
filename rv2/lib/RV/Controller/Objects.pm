package RV::Controller::Objects;
use Mojo::Base 'Mojolicious::Controller';
use POSIX qw(ceil);

sub list {
  my $c = shift;
  my $page_id = $c->param('page_id') || 1;
  my $per_page = $c->app->config->{per_page};
  $page_id--;
  my $pages = $c->qcount('movie_objects');
  
  my $objects = $c->qcustom(qq{SELECT a.id,a.name,count(b.id) as total, CONCAT(d.name,' (',c.name,')') AS contract_name
                                  FROM movie_objects a INNER JOIN movies_list b ON a.id = b.object_id
                                  INNER JOIN contracts c ON a.contract_id = c.id
                                  INNER JOIN companies d ON d.id = c.company_id
                                  GROUP BY a.id,a.name,c.name,d.name,c.company_id
                                  ORDER BY c.company_id
                                  LIMIT ?
                                  OFFSET ?},$per_page,$page_id * $per_page);
  my $count = ceil($pages/$c->app->config->{per_page});
  # если кол-во больше 1 - генерим список
  $pages = [1..$count] if $count > 1;
  
  $objects = $objects->hashes->to_array;
  $c->render(json => { list => $objects, pages => $pages });
}

sub add_new {
  my $c = shift;
  my $vars = $c->req->json;
  my $object_id;
  # если уже есть объект обновляем
  if (defined $vars->{id})
  {
    $object_id = $vars->{id};
    $c->qupdate('movie_objects',{name => $vars->{name},
                               contract_id => $vars->{contract_id} }, {id => $object_id});
   # сбрасываем номер объекта (позже запишем из массива значений)
   $c->qupdate('movies_list',{object_id => $object_id},{object_id => 0});
   say $c->qerror if $c->qerror;
  }
  # создаем объект
  else
  {
    $object_id = $c->qinsert('movie_objects',{name => $vars->{name},
                               contract_id => $vars->{contract_id} });
  }
  
  return $c->render(json => {status => 'error', contract_id => $c->qerror}) unless $object_id;
  # обновляем movies
  for my $video_id(@{$vars->{videos}})
  {
    $c->qupdate('movies_list',{id => $video_id},{object_id => $object_id});  
  }
  
  if ($c->qerror)
  {
    $c->render(json => {status => 'error', object_id => $c->qerror});
  }
  else
  {
    $c->render(json => {status => 'ok', object_id => $object_id});
  }
}

sub filter {
  my $c = shift;
  my $search = $c->param('search')  || undef;

  return  $c->render(json => {}) unless $search;
  $search = '%'.$search.'%';
  my $movies = $c->qcustom(qq{ SELECT a.id, a.video_name, a.video_id, a.channel_name,
                               b.name AS "object_name", a.object_id
                               FROM movies_list a LEFT JOIN movie_objects b ON a.object_id = b.id 
                               WHERE a.video_name LIKE ?
                               ORDER BY a.object_id ASC LIMIT 50 }, $search );
  say $c->qerror;
  $movies = $movies->hashes->to_array;
  $c->render(json => $movies);
  
}

sub edit {
  my $c = shift;
  my $obj_id = $c->param('id');
  # данные по объекту
  my $object =  $c->qselect('movie_objects',{id => $obj_id});
  my $movies = $c->qcustom(qq{ SELECT a.id, a.video_name, a.video_id, a.channel_name,
                               b.name AS "object_name", a.object_id
                               FROM movies_list a LEFT JOIN movie_objects b ON a.object_id = b.id 
                               WHERE a.object_id = ?},$obj_id);
  say $c->qerror;
  $movies = $movies->hashes->to_array;
  $c->render(json => { table => $object, movies => $movies });
}

sub delete {
  my $c = shift;
  my $obj_id = $c->param('id');
  $c->qdelete('movie_objects',{id => $obj_id});
  $c->qupdate('movies_list',{object_id => $obj_id},{object_id => undef});
  say $c->qerror if $c->qerror;
  return $c->render(status => 500, json => { message => 'Ошибка!'}) if $c->qerror;
  
  my $page_id = $c->param('page_id') || 1;
  my $per_page = $c->app->config->{per_page};
  $page_id--;
  my $pages = $c->qcount('movie_objects');
  
  my ($objects) = $c->qselect('movie_objects',{},{ order_by => { desc => 'id' },
                                         limit => $per_page,
                                         offset => $page_id * $per_page });
  my $count = ceil($pages/$c->app->config->{per_page});
  
  # если кол-во больше 1 - генерим список
  $pages = [1..$count] if $count > 1;
  
  $c->render(json => {list => $objects,  pages => $pages, message => 'Успешно удалено!'});
}

sub contracts {
  my $c = shift;
  my ($contracts) = $c->qcustom(qq{SELECT b.id, CONCAT(a.name, ' (',b.name,')') as name
                                FROM companies a INNER JOIN contracts b 
                                ON a.id=b.company_id
                                WHERE b.date_end >= now()
                                ORDER BY a.id});
  $contracts = $contracts->hashes->to_array;
  $c->render(json => $contracts);  
}
1;
