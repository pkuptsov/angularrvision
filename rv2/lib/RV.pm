package RV;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;
  $self->plugin('Config');
  $self->plugin('RV::Plugin::Validate');
  $self->plugin('RV::Plugin::Helpers');
  $self->plugin(Minion => {Pg => $self->config('minion')});
  $self->plugin('RV::Task::FileProc');
  $self->plugin('Mojolicious::Plugin::QuickPg' =>  {dsn => $self->config('pg'), debug => 1});
  $self->plugin('RenderFile');
  
  $self->secrets($self->config('secrets'));

  $self->hook(after_dispatch  => sub {
    my $c = shift;
    $c->res->headers->cache_control('max-age=1, no-cache');
    $c->res->headers->last_modified('Sun, 17 Aug 2008 16:27:35 GMT');
    $c->res->headers->server('Apache/2.4.9 (Unix)');
    
  });

  # Router
  my $r = $self->routes;
  
  # Normal route to controller
  $r->get('/login')->to('login#login');
  $r->get('/notice/:error')->to('login#error');
  $r->post('/login')->to('login#signin');
  # non authority!
  $r->get('/api/reports/format_pdf/:pass/:id')->to('reports#format_pdf');
  
  # protected area
  my $p = $r->under(sub {
    my $apps = shift;
    my $role = $apps->session('role') || 0;
    
      if ($role) {
        if (!$apps->req->headers->referrer || 
             $apps->url_for( $apps->req->headers->referrer )->host ne $self->config('host') 
           ){
            my $email = $apps->session('email');
            my $remote_ip = $apps->req->headers->header('X-Real-IP')
                         || $apps->req->headers->header('X-Forwarded-For')
                         || $apps->tx->original_remote_address
                         ||  $apps->tx->remote_address;
                    
            $apps->logger($email, $remote_ip, 'false');
        }
        return 1;
      }
      else {
        $apps->redirect_to('/login.html') and return 0;
      }
      
   });
  
  $p->get('/logout')->to('login#logout');
  
  $p->get('/')->to('users#welcome');
  
  ## API ##

  # users
  $p->get('/api/users')->to('users#get_users');
  $p->post('/api/users')->to('users#save_user');
  $p->post('/api/update//users')->to('users#update_user');
  $p->post('/api/delete/users/:user_id')->to('users#delete_user');
  
  # companies 
  $p->get('/api/companies/:company_id' => {company_id => undef})->to('companies#get_companies');
  $p->post('/api/companies')->to('companies#save_company');
  $p->post('/api/update/companies')->to('companies#update_company');
  $p->post('/api/delete/companies/:company_id')->to('companies#delete_company');

  # accounts
  $p->get('/api/banks/:company_id/:bank_id' => {bank_id => undef})->to('banks#get_banks');
  $p->post('/api/banks')->to('banks#save_bank');
  $p->post('/api/banks')->to('banks#update_bank');
  $p->post('/api/banks/:company_id/:bank_id')->to('banks#delete_bank');

  # contracts
  $p->get('/api/contracts/:company_id/:contract_id' => {contract_id => undef, company_id => undef })->to('contracts#get_contracts');
  $p->post('/api/contracts')->to('contracts#save_contract');
  $p->post('/api/update/contracts/')->to('contracts#update_contract');
  $p->post('/api/delete/contracts/:company_id/:contract_id')->to('contracts#delete_contract');
  
  # logs
  $p->get('/api/logs/:page_id' => {page_id => 1})->to('logs#get_logs');
  $p->get('/api/mel/:page_id' => {page_id => 1})->to('logs#movies_error_log');
  # rates
  $p->get('/api/rates/:page_id' => {page_id => 1})->to('rates#get_rates');
  # Import files
  $p->post('/api/import/file/save')->to('import#save_file');
  $p->get('/api/import/file/list')->to('import#list_files');
  $p->post('/api/delete/import/file/:file_id')->to('import#delete_file');
  $p->post('/api/process/file/:file_id')->to('import#process_file');
  $p->post('/api/progress/file/:file_id')->to('import#progress_file');
  $p->post('/api/process/req/:file_id')->to('import#api_req');
  $p->post('/api/statistic/file/:file_id')->to('import#statistic');
  # movies
  $p->get('/api/movies/list/:page_id' => {page_id => 1})->to('import#movies');
  # reports
  $p->post('/api/reports/report1')->to('reports#report1');
  $p->get('/api/reports/download_pdf/:file' => {id => 1})->to('reports#download_pdf');
  # objects
  $p->post('/api/objects/filter')->to('objects#filter');
  $p->get('/api/objects/contracts' )->to('objects#contracts');
  $p->post('/api/objects/add_new')->to('objects#add_new');
  $p->get('/api/objects/list/:page_id' => {page_id => 1})->to('objects#list');
  $p->any('/api/objects/edit/:id')->to('objects#edit');
  $p->post('/api/objects/delete/:id')->to('objects#delete');
  
}

1;
