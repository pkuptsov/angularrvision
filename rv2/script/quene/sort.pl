#!/usr/bin/perl -w

use common::sense;
use Data::Dumper;
use Mojo::Pg;

my $pg = Mojo::Pg->new('postgresql://rvision:Wwwrus2@localhost:5432/report_rvision');

open(FILE,'<','YouTube_rawdata.csv') or die $!;
    my $i = 0;
    #my %Lines;
    while (my $line = <FILE>) {
      my %Lines;
      #say $line;
      chomp;
      $i++;
      next if $line =~ /^Video/;
      
        ($Lines{Video_ID},$Lines{Custom_ID}, $Lines{Day},$Lines{Country},$Lines{Content_Type},$Lines{Policy},$Lines{Total_Views},
        $Lines{Watch_Page_Views},$Lines{Embedded_Player_Views},$Lines{Channel_Page_Video_Views},$Lines{Live_Views},$Lines{Recorded_Views},
        $Lines{Ad_Enabled_Views},$Lines{Ad_Requested_Views},
        $Lines{Total_Earnings},$Lines{Gross_YouTube_sold_Revenue},$Lines{Gross_Partner_sold_Revenue},$Lines{Gross_AdSense_sold_Revenue},
        $Lines{Net_YouTube_sold_Revenue},$Lines{Net_AdSense_sold_Revenue}) = split(/,/,$line);

    say $i . ":" . $Lines{Video_ID};
    #eval {
    $pg->db->query(qq{
        INSERT INTO batch_lines(
             "Video_ID", "Custom_ID", "Day", "Country", "Content_Type", 
            "Policy", "Total_Views", "Watch_Page_Views", "Embedded_Player_Views", 
            "Channel_Page_Video_Views", "Live_Views", "Recorded_Views", "Total_Earnings", 
            "Gross_YouTube_sold_Revenue", "Gross_Partner_sold_Revenue", "Gross_AdSense_sold_Revenue", 
            "Net_YouTube_sold_Revenue", "Net_AdSense_sold_Revenue")
        VALUES (?, ?, ?, ?, ?, 
            ?, ?, ?, ?, 
            ?, ?, ?, ?, 
            ?, ?, ?, 
            ?, ?)},
            $Lines{Video_ID}, $Lines{Custom_ID}, $Lines{Day}, $Lines{Country}, $Lines{Content_Type},
            $Lines{Policy},$Lines{Total_Views},$Lines{Watch_Page_Views},$Lines{Embedded_Player_Views},
            $Lines{Channel_Page_Video_Views},$Lines{Live_Views},$Lines{Recorded_Views}, $Lines{Total_Earnings},
            $Lines{Gross_YouTube_sold_Revenue},$Lines{Gross_Partner_sold_Revenue},$Lines{Gross_AdSense_sold_Revenue},
            $Lines{Net_YouTube_sold_Revenue},$Lines{Net_AdSense_sold_Revenue});
    #};
      #say $@ if $@;
     }

    #}
