#!/usr/bin/env perl

use common::sense;
use Mojo::Pg;
use Data::Dumper;
use Mojo::UserAgent;

use constant VIDEO_URL => 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=AIzaSyBn8yBc9nyA1vcO_gUeA8gxKXZn-ZpdkEk';

my $raw = Mojo::Pg->new('postgresql://rvision:Wwwrus2@localhost:5433/report_rvision');
$raw         = $raw->options({AutoCommit => 1, RaiseError => 1, PrintError => 1});

my $movies = $raw->db->query( qq{SELECT distinct "Video_ID" 
                                   FROM movies 
                                  WHERE coalesce(jsonb_array_length(errors),0) < 3
                                    AND "Video_Name" IS NULL} 
                            );



my $ua = Mojo::UserAgent->new();
 
while(my $next = $movies->hash){

    my $url = sprintf(VIDEO_URL, $next->{Video_ID});
    warn "\n\n processing $next->{Video_ID} \n\n";
    my $res = $ua->get($url)->res;
    
    my $res_json = $res->json;

    unless($res_json->{items}[0]->{snippet}->{title} &&
           $res_json->{items}[0]->{snippet}->{channelTitle}
           ){
            warn "Error on fetching url: $url content => $_[0]";
            
            $raw->db->query(qq{UPDATE movies 
                                  SET "errors" = coalesce(errors, '[]')::jsonb || coalesce(?, '"unknown error"')::jsonb 
                                WHERE "Video_ID" = ?
                               },
                            $res->body,
                            $next->{Video_ID}
                            );   
            
            next;
    }
    warn "\n\n save Video name for $next->{Video_ID} \n\n";
    $raw->db->query(qq{UPDATE movies 
                          SET "Video_Name" = ?, 
                              channelname = ?
                        WHERE "Video_ID" = ?},
                    $res_json->{items}[0]->{snippet}->{title}, 
                    $res_json->{items}[0]->{snippet}->{channelTitle}, 
                    $next->{Video_ID}
                    );                 
                    
}

$raw->db->query( qq{REFRESH MATERIALIZED VIEW mv_movies} );   

