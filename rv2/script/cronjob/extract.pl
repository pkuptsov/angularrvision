#!/usr/bin/perl -w

use common::sense;
use LWP::Simple;
use Mojo::Pg;
use Data::Dumper;
use FindBin qw($Bin);
use Mojo::Base 'Mojolicious::Plugin';
use File::Spec::Functions 'file_name_is_absolute';
use Mojo::Util qw(decode slurp);
use Date::Simple qw(date today);

my ( $flag, $str, $rate );
my $date = today();
my $date_to_fetch = $date->format("%d/%m/%Y");# $date->format("%Y%m%d")
my $url = "http://www.cbr.ru/currency_base/D_print.asp?date_req=$date_to_fetch";
my $config = my_load($Bin . '/../../r_v.conf');

my $content = striptohtml( get($url) );

for my $line(split(/\n/,$content)) {
	$flag = 1 if ($line =~ /^\<tr/);
	$flag = 0 if ($line =~ /^\<\/tr/);
	unless($flag) {
		if($str =~ /\>840\</g) {
		$str =~ /(\d+\D\d+)<\/td>/g;
		$rate = $1;
		$rate =~ s/,/./g;
		}
		
		$str=''; 
	}
	$str .= $line if ($flag);
}

my $pg = Mojo::Pg->new($config->{pg});
my $result = eval { $pg->db->query(qq{INSERT INTO rates(date, currency, rate)
										VALUES (?, ?, ?) returning id;
									}, $date, 'USD', $rate)->hash->{id};
};

say $result ? $result : $@;

## Subs
sub striptohtml
{
    ($_) = @_;
    s/\n//g;
    s/\t//g;
    s/\s\s+/ /g;
    s/\</\n\</sg;
    s/\>/\>\n/sg;
    return($_);
}


sub my_load {
  my ($file, $conf) = @_;
  return parse(decode('UTF-8', slurp $file), $file, $conf);
}

sub parse {
  my ($content, $file, $conf) = @_;

  # Run Perl code in sandbox
  my $config = eval 'package Mojolicious::Plugin::Config::Sandbox; no warnings;'
    . "use Mojo::Base -strict; $content";
  die qq{Can't load configuration from file "$file": $@} if $@;
  die qq{Configuration file "$file" did not return a hash reference.\n}
    unless ref $config eq 'HASH';

  return $config;
}
